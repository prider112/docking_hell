Docking hell

Objetivo
- Crear tres servidores distintos en tres lenguajes diferentes y hacerles funcionar en docker

Lenguajes
- Python
- Golang
- Java script

Procedimiento
1. Se crearan las carpetas necesarias para el desarrollo del proyecto
2. Dentro de las carpetas se crearan los programas necesarios para el respectivo servidor y que estos regresen un "hello world" al funcionar
3. Se probaran los programas por individual
4. Se creara el archivo nginx
5. Se creara un archivo docker dentro de cada carpeta de programa
6. Se probara cada programa utilizando docker

Nota: Se creara el archivo docker de manera manual para conocer a fondo su funcionamiento